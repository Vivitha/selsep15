package testcases;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class DuplicateLead extends ProjectMethod{

		@BeforeTest
		public void setData() {
				testCaseName ="Find Lead";
				testDesc="Find Lead Description";
				author="Vivitha";
				category="Smoke";
		}
		//@Test(invocationCount=2,timeOut=15000)
		@Test
		public void duplicateLead()
		{
		WebElement link1 = locateElement("link","Leads");
		click(link1);
		WebElement findLead=locateElement("xpath","//div[@class='frameSectionBody']/ul/li[3]/a");
		click(findLead);
		List<WebElement> LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
	    List<WebElement> LeadIDs=new ArrayList<>();
	    for (WebElement eachElement : LId) {
				LeadIDs.add(eachElement);	
			}
	    WebElement LeadID=LeadIDs.get(0);
	    clickWitOutSnap(LeadID);
	   WebElement DuplicateLead=locateElement("xpath","//a[text()='Duplicate Lead']");
	   click(DuplicateLead);
	   WebElement phone=locateElement("id","createLeadForm_primaryPhoneNumber");
	   phone.clear();
	   type(phone,"9159561649");
	   WebElement link4 = locateElement("xpath","//input[@class='smallSubmit']");
		click(link4);
		}
	}


package week2.day1;

public interface Entertainment
{

	public void brand();
	public void screensize();
	public void picturequality();
}
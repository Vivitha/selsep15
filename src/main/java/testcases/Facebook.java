package testcases;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class Facebook extends ProjectMethod{
	@BeforeTest
	public void setData() {
			testCaseName ="Facebook TestCase";
			testDesc="FaceBook Description";
			author="Vivitha";
			category="Smoke";
	}
	@Test
public void facebookTestCase() throws InterruptedException
{
		WebElement Search=locateElement("xpath","//input[@class='_1frb']");
		Search.clear();
		type(Search,"TestLeaf");
		WebElement SearchClick=locateElement("xpath","//button[@data-testid='facebar_search_button']/i[1]");
		click(SearchClick);
		WebElement Liked=locateElement("xpath","//button[contains(@class,'PageLikedButton')]");
		System.out.println(Liked);
		if (Liked == null)
		{
			Liked=locateElement("xpath","//button[contains(@class,'PageLikeButton')]");
			click(Liked);
			WebElement linkclick=locateElement("xpath","//div[text()='TestLeaf']");
			click(linkclick);
			
		}
		else
		{
			WebElement linkclick=locateElement("xpath","//div[text()='TestLeaf']");
			click(linkclick);
		}
		/*WebElement linkclick=locateElement("xpath","//div[text()='TestLeaf']");
		click(linkclick);*/
		WebElement Review=locateElement("xpath","//span[text()='Reviews']");
		click(Review);
	
		
}
}

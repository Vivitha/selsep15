package week1.day2;

import java.util.Scanner;

public class LearningArray {

	public static void main(String[] args) {
		//static array
		// TODO Auto-generated method stub
		int salary[] = {100000,200000,300000};
		for(int i=0;i<3;i++) {
			System.out.println("Display Salary:" + salary[i]);
		}
	//dynamic array
		System.out.println("Enter the salaries: ");
	int salaries[]=new int[4];
    Scanner s1=new Scanner(System.in);
    for(int i=0;i<salaries.length;i++)
    {
    salaries[i] = s1.nextInt();
    }
    for(int i=0;i<salaries.length;i++)
    {
    	System.out.println(salaries[i]);
    }
    //dynamic size and value and for each
   
    System.out.println("Enter the index: ");
    int size=s1.nextInt();
    int sal[] = new int[size];
    for(int i=0;i<sal.length;i++)
    {
    sal[i] = s1.nextInt();
    }
    for(int salss:sal)
    {
    	System.out.println(salss);
    }
    //sum of odd numbers
    System.out.println("Enter the index for the odd sum");
    int indodd = s1.nextInt();
    int sum=0;
    int oddnum[]=new int[indodd];
    System.out.println("Enter the number: ");
    for(int i=0;i<oddnum.length;i++)
    {
    oddnum[i] = s1.nextInt();
    }
    
    for(int i=0;i<oddnum.length;i=i+1)
    {
    	if(oddnum[i]%2!=0)
    	{
    	sum = sum + oddnum[i];
    	}
    }
    
    System.out.println("sum of odd numbers:" + sum);
    }

}

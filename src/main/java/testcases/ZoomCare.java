package testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class ZoomCare extends ProjectMethod {
	@BeforeTest
	public void setData() {
			testCaseName ="ZoomCar";
			testDesc="Zoom Car Description";
			author="Vivitha";
			category="Smoke";
	}
	@Test
	public void car()
	{
		WebElement WJourney = locateElement("xpath","//a[text()='Start your wonderful journey']");
		click(WJourney);
		WebElement Place = locateElement("xpath","//div[contains(text(),'Thuraipakkam')]");
		click(Place);
		verifySelected(Place);
		WebElement NextButton = locateElement("xpath","//button[text()='Next']");
		 click(NextButton);
		 Date date=new Date();
		 DateFormat sdf=new SimpleDateFormat("dd");
		 String today=sdf.format(date);
		 int Tomorrow=Integer.parseInt(today)+1;
		 System.out.println("Tomorrow Date:"+Tomorrow);
		 WebElement days = locateElement("xpath","//div[contains(text(),'"+Tomorrow+"')]");
		 click(days);
		 WebElement bt1 = locateElement("xpath","//button[text()='Next']");
		 click(bt1);
		 WebElement verifyDate = locateElement("xpath","//button[text()='Done']");
		 click(verifyDate);
		 //get the Rate of the car
		 List<WebElement> priceElement = driver.findElementsByXPath("//div[@class='price']");
		 System.out.println(priceElement);
		 List<String> price = new ArrayList<>();
		 List<Integer> prc = new ArrayList<>();
		 List<String> newPrice = new ArrayList<>();
		 for (WebElement eachElement : priceElement) {
				price.add(eachElement.getText());	
			}
			System.out.println("Price List"+price);
			for (String rate : price) {
				newPrice.add(rate.replaceAll("[^0-9]", ""));
			}
			for(String r:newPrice) {
				prc.add(Integer.parseInt(r));
				
			}
			System.out.println(prc);
			Collections.sort(prc);
					
			int maxprice=prc.get(prc.size()-1);
			System.out.println(maxprice);
			String brandName=locateElement("xpath","//div[contains(text(),'"+maxprice+"')]/preceding::h3[1]").getText();
			System.out.println(brandName);
	}
	
}

package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC001Login extends SeMethods{

	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement link = locateElement("linktext","CRM/SFA");
		click(link);
		WebElement link1 = locateElement("linktext","Leads");
		click(link1);
		WebElement link2 = locateElement("linktext","Create Lead");
		click(link2);
		WebElement eleCompany=locateElement("id","createLeadForm_companyName");
		type(eleCompany,"Cognizant");
		WebElement eleFname=locateElement("id","createLeadForm_firstName");
		type(eleFname,"Vivitha");
		WebElement eleLname=locateElement("id","createLeadForm_lastName");
		type(eleLname,"Jayakumar");
		WebElement link4 = locateElement("xpath","//input[@class='smallSubmit']");
		click(link4);
		
					System.out.println("updated by vinoth");
					
}
	
}













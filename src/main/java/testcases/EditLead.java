package testcases;
	import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

	public class EditLead extends ProjectMethod{
		@BeforeTest(groups="any")
		public void setData() {
				testCaseName ="Edit Lead";
				testDesc="Edit Lead Description";
				author="Vividtha";
				category="Smoke";
		}

		//@Test(dependsOnMethods= {"testcases.Lead.create"},alwaysRun=true)
		//@Test(groups="sanity")
		//@Test(groups={"sanity"},dependsOnGroups= {"smoke"})
		//@Test
		@Test(invocationCount=10)
		public void editLead() {
			WebElement link1 = locateElement("link","Leads");
			click(link1);
			WebElement findLead=locateElement("xpath","//div[@class='frameSectionBody']/ul/li[3]/a");
			click(findLead);
		   //WebElement LeadId=locateElement("xpath","//label[text()='Lead ID:']/following::input[1]");
		   //type(LeadId,"10016");
		   //WebElement FLead=locateElement("xpath","//button[text()='Find Leads']");
		   //click(FLead);
			List<WebElement> LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
		    List<WebElement> LeadIDs=new ArrayList<>();
		    for (WebElement eachElement : LId) {
					LeadIDs.add(eachElement);	
				}
		    WebElement LeadID=LeadIDs.get(0);
		    clickWitOutSnap(LeadID);
			
			WebElement editClick = locateElement("xpath", "//a[text()='Edit']");
			click(editClick);
			WebElement editLeadScreen = locateElement("xpath", "//div[text()='Edit Lead']");
			verifyExactText(editLeadScreen, "Edit Lead");
			
			WebElement descText = locateElement("updateLeadForm_description");
			type(descText, "This is to edit already created lead");
			
			WebElement updateLead = locateElement("xpath", "//input[@value='Update']");
			click(updateLead);
			
			WebElement viewLeadScreen = locateElement("xpath", "//div[text()='View Lead']");
			verifyExactText(viewLeadScreen, "View Lead");
			
		}
	}


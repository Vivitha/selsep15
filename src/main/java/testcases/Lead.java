package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excelTestCase.ReadExcel;
import wdMethods.ProjectMethod;

public class Lead extends ProjectMethod{
	
	@BeforeTest(groups="any")
	public void setData() {
			testCaseName ="Create Lead";
			testDesc="Create Lead Description";
			author="Vivitha";
			category="Smoke";
	}
   // @Test(invocationCount=2,timeOut=15000)
	//@Test(groups="smoke"

	@DataProvider(name="qa",indices= {2})
	public Object[][] fetchdata() throws IOException
	{
		/*Object[][] data=new Object[2][3];
		data[0][0]="RBS";
		data[0][1]="Thanuja";
		data[0][2]="Padmanban";
		data[0][3]=9159561649L;
		
		data[1][0]="CTS";
		data[1][1]="Visrutha";
		data[1][2]="Murali";
		data[1][3]=9159561649L;*/
		Object[][] data=ReadExcel.readexcel();
		
		return data;
		
	}
	@Test(dataProvider="qa",groups="smoke",invocationCount=10)
    public void create(String company, String fname, String lname, String ph) throws InterruptedException
    {
        WebElement link1 = locateElement("link","Leads");
		click(link1);
		WebElement link2 = locateElement("link","Create Lead");
		click(link2);
		WebElement eleCompany=locateElement("id","createLeadForm_companyName");
		//type(eleCompany,"Cognizant");
		type(eleCompany,company);
		WebElement eleFname=locateElement("id","createLeadForm_firstName");
	//	type(eleFname,"Vivitha");
		type(eleFname,fname);
		WebElement eleLname=locateElement("id","createLeadForm_lastName");
	//	type(eleLname,"Jayakumar");
		type(eleLname,lname);
		WebElement eleSource=locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource,"Cold Call");
		WebElement eleParentAccount=locateElement("xpath","//input[@id='createLeadForm_parentPartyId']/following::img");
		click(eleParentAccount);
		switchToWindow(1);
		WebElement select=locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
		clickWitOutSnap(select);
		switchToWindow(0);
		WebElement createFirstName=locateElement("id","createLeadForm_firstNameLocal");
		type(createFirstName,"Vasundara");
		WebElement eleMarket=locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleMarket,"Affiliate Sites");
		WebElement CreateLastName=locateElement("id","createLeadForm_lastNameLocal");
		type(CreateLastName,"Viswalingam");
		WebElement Salution=locateElement("id","createLeadForm_personalTitle");
		type(Salution,"xxx");
		WebElement phone=locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phone,""+ph);
		WebElement link4 = locateElement("xpath","//input[@class='smallSubmit']");
		click(link4);

	}

}

package testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class FindLeads extends ProjectMethod{
	@BeforeTest
	public void setData() {
			testCaseName ="Find Lead";
			testDesc="Find Lead Description";
			author="Vivitha";
			category="Smoke";
	}
	//@Test(invocationCount=2,timeOut=15000)
	@Test
	public void create2()
	{
	WebElement link1 = locateElement("link","Leads");
	click(link1);
	WebElement findLead=locateElement("xpath","//div[@class='frameSectionBody']/ul/li[3]/a");
	click(findLead);
   //WebElement LeadId=locateElement("xpath","//label[text()='Lead ID:']/following::input[1]");
   //type(LeadId,"10016");
   //WebElement FLead=locateElement("xpath","//button[text()='Find Leads']");
   //click(FLead);
	List<WebElement> LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
    List<WebElement> LeadIDs=new ArrayList<>();
    for (WebElement eachElement : LId) {
			LeadIDs.add(eachElement);	
		}
    WebElement LeadID=LeadIDs.get(0);
    clickWitOutSnap(LeadID);
   //WebElement SelectLead=locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
   //click(SelectLead);
	}
}

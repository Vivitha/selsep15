package testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class MergeLead extends ProjectMethod{
	@BeforeTest(groups="any")
	public void setData() {
			testCaseName ="Merge Lead";
			testDesc="Merge Lead Description";
			author="Vivitha";
			category="Smoke";
	}
	//@Test(dependsOnMethods= {"testcases.Lead.create"},alwaysRun=true)
	 //@Test(groups="regression")
	@Test(groups={"regression"},dependsOnGroups= {"sanity"})
	//@Test
	public void Merge()
	{
	
			WebElement link1 = locateElement("xpath","//a[text()='Leads']");
			click(link1);
	       WebElement MergeLead=locateElement("xpath","//a[text()='Merge Leads']");
	       click(MergeLead);
	       WebElement MergeSearch=locateElement("xpath","//input[@id='partyIdFrom']/following-sibling::a/img");
	       click(MergeSearch);
	       switchToWindow(1);
	       List<WebElement> LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
	       List<WebElement> LeadIDs=new ArrayList<>();
	       for (WebElement eachElement : LId) {
				LeadIDs.add(eachElement);	
			}
	       WebElement LeadID=LeadIDs.get(0);
	       clickWitOutSnap(LeadID);
	       switchToWindow(0);
	       WebElement ToLead=locateElement("xpath","//input[@id='partyIdTo']/following-sibling::a/img");
	       click(ToLead);
	       switchToWindow(1);
	       LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
	       LeadIDs=new ArrayList<>();
	       for (WebElement eachElement : LId) {
				LeadIDs.add(eachElement);	
			}
	       LeadID=LeadIDs.get(1);
	       clickWitOutSnap(LeadID);
	       switchToWindow(0);
	       WebElement Merge=locateElement("class","buttonDangerous");
	       clickWitOutSnap(Merge);
	       acceptAlert();
	       
	}

}

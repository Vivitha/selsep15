package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class AlternativeIndex {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> l1=new ArrayList<>();
		l1.add(1);
		l1.add(2);
		l1.add(3);
		l1.add(4);
		l1.add(7);
		l1.add(8);
		System.out.println("BEFORE COPYING");
		for(int num:l1)
		{
			System.out.println(num);
		}
		List<Integer> l2=new ArrayList<>();
		for(int num1:l1)
		{
			int odd=l1.indexOf(num1);
			if(odd%2==0) {
			int value=l1.get(odd);
			l2.add(value);
			}
			
		}
		System.out.println(l2);

	}

}


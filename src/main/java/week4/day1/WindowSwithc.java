package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowSwithc {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//invoke chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//maxmize the window
		driver.manage().window().maximize();
		
		//load the url
			driver.get("https://www.irctc.co.in/nget/train-search");
            driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
            driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
            driver.findElementByLinkText("Contact Us").click();
            Set<String> windowHandles = driver.getWindowHandles();
            List<String> lst=new ArrayList<>();
            lst.addAll(windowHandles);
            driver.switchTo().window(lst.get(1));
            File src=driver.getScreenshotAs(OutputType.FILE);
            File obj=new File("./snaps/img1.jpeg");
            FileUtils.copyFile(src, obj); 
            Set<String> windowHandles1 = driver.getWindowHandles();
            List<String> lst1=new ArrayList<>();
            lst1.addAll(windowHandles1);
            driver.switchTo().window(lst.get(0));
            driver.close();
                       
            
	}
	

}

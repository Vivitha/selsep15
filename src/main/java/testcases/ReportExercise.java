package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportExercise {
@Test
	public void report() throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger=extent.createTest("Lead","Create Lead");
		logger.assignAuthor("Vivitha");
		logger.assignCategory("Smoke");
		logger.log(Status.PASS,"The Data DemoSalesManager Entered Successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img4.png").build());
		logger.log(Status.PASS,"The Data CRMSFA Entered Successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img5.png").build());
		logger.log(Status.PASS,"The Login Button Successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img6.png").build());
		extent.flush();
	}

}


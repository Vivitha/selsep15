package testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class DeleteLead extends ProjectMethod{

	@BeforeTest(groups="any")
	public void setData() {
			testCaseName ="Delete Lead";
			testDesc="Delete Lead Description";
			author="Vivitha";
			category="Smoke";
	}
	//@Test
	@Test(invocationCount=10)
	public void deleteLead()
	{
		WebElement link1 = locateElement("link","Leads");
		click(link1);
		WebElement findLead=locateElement("xpath","//div[@class='frameSectionBody']/ul/li[3]/a");
		click(findLead);
		List<WebElement> LId=driver.findElementsByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a[1]");
	    List<WebElement> LeadIDs=new ArrayList<>();
	    for (WebElement eachElement : LId) {
				LeadIDs.add(eachElement);	
			}
	    WebElement LeadID=LeadIDs.get(0);
	    clickWitOutSnap(LeadID);
	    WebElement deleteLead=locateElement("xpath","//a[text()='Delete']");
	    click(deleteLead);
	}
}

package testcases;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

public class ListernerMethod implements IAnnotationTransformer, IRetryAnalyzer {
	int maxRetry=0;
	@Override
	public boolean retry(ITestResult result) {
		if(maxRetry<2)
		{
			maxRetry++;
			return true;
		}
		else
		{
		return false;
	}
	}
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		// TODO Auto-generated method stub
		if(!testMethod.getName().equals("create"))
		{
			annotation.setEnabled(false);
		}
		annotation.setInvocationCount(1);
		annotation.setRetryAnalyzer(this.getClass());
		
	}

}

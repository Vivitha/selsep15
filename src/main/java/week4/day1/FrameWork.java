package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameWork {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.W3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert prompt=driver.switchTo().alert();
		prompt.sendKeys("vivitha");
		prompt.accept();
		Thread.sleep(4000);
		String nme = driver.findElementById("demo").getText();
		if(nme.contains("vivitha"))
		{
			System.out.println(nme.contains("vivitha")+"display");
		}
		Thread.sleep(4000);
		driver.switchTo().defaultContent();
	
		

	}

}

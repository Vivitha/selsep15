package week2.day2;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SetRemoveDup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int  [] ch1= {1,2,2,6,7,3,4,4,5,5,6,7};
		Map<Integer,Integer> mi=new LinkedHashMap<>();
		int count=1;
		for(Integer i:ch1)
		{
			if(!mi.containsKey(i))
			{
				mi.put(i,count);
			}
			else
			{
				count=count+1;
				mi.put(i,count);
			}
			count=1;
		}
		
		for(Entry<Integer,Integer> iop:mi.entrySet())
		{
			System.out.println(iop.getKey()+"--->"+iop.getValue());
		}

	}

}

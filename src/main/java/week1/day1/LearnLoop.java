package week1.day1;

public class LearnLoop {
	public void learnWhileLoop()
	{
		int i = 1;
		int sum = 0;
		while(i>=5)
		{
			sum = sum + i;
			i++;
		}
		System.out.println("sum of the number using while loop"+ sum);
	}
public void learnDoWhile() {
	int i = 1;
	int sum =0;
	do
	{
		sum = sum + i;
		i++;
		
	}while(i>=5);
	System.out.println("sum of the number using do while loop"+ sum);
}
public void learnForLoop() {
	int sum=0;
	for(int i =1;i<=5;i=i+1) {
		sum = sum + i;
		
	}
   System.out.println("sum of the number using for loop"+sum);
}
public static void main(String[] args) {
	LearnLoop obj2=new LearnLoop();
	obj2.learnDoWhile();
	obj2.learnWhileLoop();
	obj2.learnForLoop();
}
}

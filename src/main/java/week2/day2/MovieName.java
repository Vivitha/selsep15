package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MovieName {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MovieName obj = new MovieName();
		obj.reverseord();
		obj.moviecount();
		obj.oddnumber();
	
	}
//	Using list get the bahuballi duplicate count
	public void moviecount()
	{
	int sum=0;
	List <String> movies=new ArrayList<>();
	movies.add("Saami");
	movies.add("bahuballi");
	movies.add("Run");
	movies.add("Strawberry");
	movies.add("Kanchana");
	movies.add("bahuballi");
	System.out.println("Number of movies:"+movies.size());
	for (String movname : movies) {
		
		if (movname == "bahuballi") {
			sum++;
		}
		
	}
	System.out.println();
	
	System.out.println("Number of bhauballi "+sum);
	}
	//Using List print the number in reverse order
	public void reverseord()
	{
		
		
	List <Integer> number=new ArrayList<>();
	number.add(1);
	number.add(2);
	number.add(3);
	number.add(4);
	number.add(5);
	number.add(6);
	System.out.println("The number of input passed:"+ number.size());
	for(int i=number.size()-1;i>=0;i--)
	{
		System.out.print(number.get(i));
	}
	
	}
	public void oddnumber()
	{
		Set <Integer> num=new LinkedHashSet<>();
		num.add(100);
		num.add(200);
		num.add(300);
		num.add(400);
		num.add(500);
		num.add(600);
		System.out.println("The length of the number is :"+num.size());
		System.out.println("Before processing:" );
		for (int n1 : num) {
			System.out.println(n1);
		}
		List <Integer> ls=new ArrayList<>();
		ls.addAll(num);
		System.out.println("The number in odd index are: ");
		for(int i=0;i<ls.size();i++)
		{
			if(i%2!=0)
			{
				System.out.println(ls.get(i));
			}
		}
				}
}

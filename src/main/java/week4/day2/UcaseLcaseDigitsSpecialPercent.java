package week4.day2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UcaseLcaseDigitsSpecialPercent {

	public static void main(String[] args) {
	
	//String with Alphabets, Numbers and Special Characters
	String txt = "TigerRuns@TheSpeedOf100km/hour.";
	
	//Extract numbers only and count the number
	String numberOnly= txt.replaceAll("[^0-9]", "");
	int nlen=numberOnly.length();
	System.out.println("Numbers are: "+numberOnly);
	System.out.println("Number of digits : "+ nlen);
	
	//Extract lower case letters only
    String salphaOnly = txt.replaceAll("[0-9\\WA-Z]", "");
    int slen=salphaOnly.length();
    System.out.println("Lower case are :"+salphaOnly);
    System.out.println("Number of lcase : "+ slen);
    
    //Extract upper case letters only
    String calphaOnly = txt.replaceAll("[0-9\\Wa-z]", "");
    int clen=calphaOnly.length();
    System.out.println("Upper Case are: "+calphaOnly);
    System.out.println("Number of Ucase : "+ clen);
    
    //Extract Special Characters only
    String specialcharOnly = txt.replaceAll("[0-9A-Za-z]", "");
    int splen=specialcharOnly.length();
    System.out.println("Special Characters are :"+specialcharOnly);
    System.out.println("Number of other Chracters: "+ splen);
    
    //Get the length of the whole String
    int len=txt.length();
    System.out.println(len);
   
    //declare decimal variable
    double d1, d2, d3, d4;
    
    //Percentage of digits
    d1=((double)(nlen/(double)len)*(double)100);
    System.out.printf("Number of Digits is "+nlen+" so percentage is "+"%.2f",d1);
    System.out.println();
    
    //Percentage of lower case
    d2=((double)(slen/(double)len)*(double)100);
     System.out.printf("Number of lower case letter is "+slen+" so percentage is "+"%.2f",d2);
     System.out.println();
     
     //Percentage of upper case
     d3=((double)(clen/(double)len)*(double)100);
      System.out.printf("Number of Upper Case letter  is "+clen+" so percentage is "+"%.2f",d3);
      System.out.println();
      
      //Percentage of other characters
      d4=((double)(splen/(double)len)*(double)100);
       System.out.printf("Number of other characters is "+splen+" so percentage is "+"%.2f",d4);
    
	}
}

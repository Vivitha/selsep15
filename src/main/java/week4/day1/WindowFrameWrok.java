package week4.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowFrameWrok {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//invoke chrome browser
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				
				//maxmize the window
				driver.manage().window().maximize();
				
				//load the url
				driver.get("http://leaftaps.com/opentaps");
				driver.findElementById("username").sendKeys("DemoSalesManager");
				
				//Enter the Password
				driver.findElementById("password").sendKeys("crmsfa");
				
				//click login
				driver.findElementByClassName("decorativeSubmit").click();
				driver.findElementByLinkText("CRM/SFA").click();
				driver.findElementByLinkText("Create Lead").click();
				driver.findElementByLinkText("Merge Leads").click();			

	}

}

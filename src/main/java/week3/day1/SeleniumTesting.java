package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumTesting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//invoke chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		//maxmize the window
		driver.manage().window().maximize();
		
		//load the url
		driver.get("http://leaftaps.com/opentaps/");
		
		//Enter the user name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//Enter the Password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vivitha");
		driver.findElementById("createLeadForm_lastName").sendKeys("Jayakumar");
		WebElement source=driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");
		WebElement source1=driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		WebElement source2=driver.findElementById("createLeadForm_industryEnumId");
		Select dd3 = new Select(source2);
		List<WebElement> alloptions=dd3.getOptions();
		for(WebElement eachoption : alloptions)
		{
			if(eachoption.getText().startsWith("M"))
					{
				System.out.println(eachoption.getText());
				
					}
		}
		System.out.println(alloptions.size());
		driver.findElementByClassName("smallSubmit").click();
		
		
	}

}

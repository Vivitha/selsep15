package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TrainTicket {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d1=new ChromeDriver();
		d1.manage().window().maximize();
		d1.get("https://erail.in");
		d1.findElementById("txtStationFrom").clear();
		d1.findElementById("txtStationFrom").sendKeys("kpd",Keys.TAB);
		d1.findElementById("txtStationTo").clear();
		Thread.sleep(2000);
		d1.findElementById("txtStationTo").sendKeys("mas",Keys.TAB);
		Thread.sleep(2000);
		WebElement chk = d1.findElementById("chkSelectDateOnly");
		if(chk.isSelected())
		{
			chk.click();
		}
		Thread.sleep(2000);
		
		WebElement table = d1.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
/*		List<WebElement> cols = rows.findElements(By.tagName("td"));
		for(WebElement r1:rows)
		{
		
			String text = r1.getText();
			System.out.println(text);
			String text2 = cols.get(1).getText();
			System.out.println(text2);
		}*/
		for(int i=0;i<rows.size();i++)
		{
			List<WebElement> cols = rows.get(i).findElements(By.tagName("td"));
			String text = cols.get(1).getText();
			System.out.println(text);
		}
		
	}
	

}
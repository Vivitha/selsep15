package excelTestCase;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	//@Test
	public static Object[][] readexcel() throws IOException
	{
		XSSFWorkbook wb=new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet sheet=wb.getSheetAt(0);
		int rowcount=sheet.getLastRowNum();
		System.out.println("Row Count"+rowcount);
		int columncount=sheet.getRow(0).getLastCellNum();
		System.out.println("Column count"+ columncount);
		Object[][] data=new Object[rowcount][columncount];
		for(int j=1;j<=rowcount;j++)
		{
			XSSFRow row=sheet.getRow(j);
			for(int i=0;i<columncount;i++)
			{
				XSSFCell cell=row.getCell(i);
				
				try
				{
					String Value=cell.getStringCellValue();
					data[j-1][i]=Value;
					System.out.println(Value);
				}catch(NullPointerException e)
				{
					System.out.println("");
				}
			}
				
		}
		wb.close();
		return data;
		
	
	}
}
